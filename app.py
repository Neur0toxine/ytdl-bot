from __future__ import unicode_literals
import os
import re
import logging
import telebot
import threading
import random
import youtube_dl as yt
from telebot import types
from proxy import Proxies

'''
Global vars
'''
bot = telebot.AsyncTeleBot(os.environ.get('TG_BOT_APIKEY'))
uri_regex = re.compile(r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', re.I | re.M)
rutube_regex = re.compile(r'\?pl_id\=\d+\&pl_type\=.+', re.I)
proxy = Proxies()

'''
Entry point
'''
def init():
	logging.basicConfig(level=logging.INFO, format='[%(levelname)s: %(asctime)s] - %(message)s')
	logging.info('Starting up...')
	logging.info('Loading proxies list...')
	proxy.update_proxies()
	logging.info('Loaded ' + str(proxy.count()) + ' proxies.')
	try:
		logging.info('Done.')
		bot.polling()
	except KeyboardInterrupt:
		logging.info('Quit.')
		exit()
	except:
		pass

@bot.message_handler(commands=['icon'])
def icon_credits(message):
	bot.send_message(message.from_user.id,
	'Icons made by <a href="http://www.freepik.com" title="Freepik">Freepik</a> ' + 
	'from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> ' + 
	'is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" ' + 
	'title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a>', parse_mode="HTML")

@bot.message_handler(regexp=".+")
def catch_msg(message):
	urls = list(set(uri_regex.findall(message.text)))
	if len(urls) != 0:
		bot.send_message(message.from_user.id, '*Найдено ссылок:* `' + str(len(urls)) + '`.\r\n' + 
		'Ведётся обработка, пожалуйста, подождите...', disable_web_page_preview=True,
		disable_notification=True, parse_mode="Markdown").wait()
		if len(urls) > 10:
			urls = urls[:10]
			bot.send_message(message.from_user.id, 'Найдено больше 10 ссылок, будут обработаны только первые 10.',
			disable_web_page_preview=True, disable_notification=True, parse_mode="Markdown").wait()
		country = str(message.from_user.language_code[-2:])
		if country == 'en':
			country = 'US'
		with yt.YoutubeDL({
			"noplaylist": True,
			"quiet": True,
			"simulate": True,
			"geo_bypass": True,
			"geo_bypass_country": country,
			"geo_verification_proxy": proxy.by_geo(country),
			"proxy": proxy.by_geo(country)
			}) as ydl:
			for uri in urls:
				info = None
				try:
					fixed_uri = rutube_regex.sub('', uri)
					info = ydl.extract_info(fixed_uri, download=False)
				except:
					bot.send_message(message.from_user.id, 'Ссылка: `' + uri + '`\r\n' + 
					'Произошла ошибка. Возможно, сайт не поддерживается.', parse_mode="Markdown")
				else:
					text = '*Найдено видео:* _' + info['title'] + '_\r\n*Ссылка:* `' + uri + '`\r\n*Выберите формат из списка:*'
					for link in info['formats']:
						if 'format_note' in link:
							text += '\r\n[' + str(link['ext']).upper() + ' ' + link['format_note'] + '](' + link['url'] + ')'
						else:
							text += '\r\n[' + str(link['ext']).upper() + ' ' + link['format'] + '](' + link['url'] + ')'
					bot.send_message(message.from_user.id, text, parse_mode="Markdown")
			if len(urls) > 1:
				bot.send_message(message.from_user.id, 'Все ссылки обработаны!')
	else:
		bot.send_message(message.from_user.id, 
		"Этот бот поможет Вам скачать видео с множества сайтов. Полный список поддерживаемых " + 
		"сайтов вы найдёте [здесь](https://rg3.github.io/youtube-dl/supportedsites.html).\r\n" +
		"Просто отправьте мне ссылку на видео. За один раз вы можете отправить до 10 ссылок." + 
		"\r\n\r\n*Что поддерживается:*\r\n- скачивание с сайтов без HLS;\r\n- скачивание аудио.\r\n" +
		"\r\n*Что не поддерживается:*\r\n- скачивание видео по плейлисту HLS.\r\n" + 
		"\r\nПо мере возможности функционал будет дополняться.",
		parse_mode="Markdown")

init()