import logging
import threading
import time
import asyncio
import random
from proxybroker import Broker

'''
This functionality need to be in another class
'''
class Proxies:
    proxy_storage = []

    async def add_proxy_to_list(self, proxies):
        while True:
            proxy = await proxies.get()
            if proxy is None:
                break
            else:
                proxydic = {}
                proto = 'https' if 'HTTPS' in proxy.types else 'http'
                proxydic['geo'] = proxy.geo.code
                proxydic['address'] = '%s://%s:%d' % (proto, proxy.host, proxy.port)
                self.proxy_storage.append(proxydic)

    def update_proxies(self):
        self.proxy_storage = []
        try:
            proxies = asyncio.Queue()
        except RuntimeError:
            loop = asyncio.new_event_loop()
            asyncio.set_event_loop(loop)
            proxies = asyncio.Queue()
        broker = Broker(proxies)
        tasks = asyncio.gather(broker.find(types=['HTTP', 'HTTPS'], limit=100), self.add_proxy_to_list(proxies))
        loop = asyncio.get_event_loop()
        logging.debug('updating proxies list')
        loop.run_until_complete(tasks)
        logging.debug('proxies list updated, loaded ' + str(len(self.proxy_storage)) + ' proxies.')

    '''
    TODO: Fix it.
    '''
    def update_proxies_th(self):
        while True:
            logging.info('Loading new proxies...')
            self.update_proxies()
            if self.interval == 0:
                break
            time.sleep(self.interval)

    def count(self):
        return len(self.proxy_storage)

    def remove_proxy(self, address):
        self.proxy_storage = filter(lambda x: x['address'] != address, self.proxy_storage)

    '''
    Pick proxy by geocode, or pick random
    '''
    def by_geo(self, geo):
        match = [x for x in self.proxy_storage if x['geo'] == geo.upper()]
        if len(match) == 0:
            return self.proxy_storage[random.randrange(0, len(self.proxy_storage)-1)]['address']
        else:
            return match[0]['address']

    def __init__(self, auto_load=False, update_interval=0):
        logging.debug('created Proxies class')
        if auto_load:
            logging.debug('running update_proxies')
            self.update_proxies()
        self.interval = update_interval
        if update_interval > 0:
            self.thread = threading.Thread(target=self.update_proxies_th, args=())
            self.thread.daemon = True
            self.thread.start()
